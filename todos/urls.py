from django.urls import path
from todos.views import (
    create_item,
    create_list,
    del_list,
    edit_item,
    edit_list,
    list_detail,
    show_lists,
)

urlpatterns = [
    path("", show_lists, name="todo_list_list"),
    path("<int:id>/", list_detail, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("<int:id>/delete/", del_list, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_item, name="todo_item_update"),
]
