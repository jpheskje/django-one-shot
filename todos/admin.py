from django.contrib import admin
from todos.models import TodoItem, TodoList


@admin.register(TodoList)
class TodosAdmin(admin.ModelAdmin):
    list_display = ("name", "id")


@admin.register(TodoItem)
class TodosAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date", "is_completed")
