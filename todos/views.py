from django.shortcuts import get_object_or_404, redirect, render
from todos.models import TodoItem, TodoList
from todos.forms import TodoItemForm, TodoListForm

# Create your views here.


def show_lists(request):
    lists = TodoList.objects.all()
    context = {"todo_lists": lists}
    return render(request, "todos/lists.html", context)


def list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    tasks = todo_list.items.all()
    context = {"todo_list": todo_list, "tasks": tasks}
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_todo = form.save()
            return redirect("todo_list_detail", id=new_todo.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_list.html", context)


def edit_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            updated_todo = form.save()
            return redirect("todo_list_detail", id=updated_todo.id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "todo_list": todo_list,
        "form": form,
    }
    return render(request, "todos/edit_list.html", context)


def del_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/del_list.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            updated_item = form.save()
            return redirect("todo_list_detail", id=updated_item.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
